#include <vector>
#include <iomanip>
#include "encrypt_lib.cpp"

using std::string, std::cout, std::endl, std::cin,
    std::bitset, std::getline, std::ifstream,
    std::stringstream, std::ofstream, std::strlen,
    std::pow, std::map, std::vector, std::setprecision,
    std::pair, std::runtime_error, std::cerr;

unsigned char find_possible_key(const map<unsigned char, float> &char_frequencies)
{
    pair<unsigned char, float> most_common;
    pair<unsigned char, float> second_common;

    for (auto key_val : char_frequencies)
    {
        if (key_val.second > most_common.second)
        {
            most_common = key_val;
        }
    }
    for (auto key_val : char_frequencies)
    {
        if (key_val.second > second_common.second and
                key_val.first != most_common.first)
        {
            second_common = key_val;
        }
    }

    unsigned char possible_keys[2] = {
        most_common.first ^ ((unsigned char) 32),
        second_common.first ^ ((unsigned char) 238) // код буквы о
    };
    if (possible_keys[0] == possible_keys[1])
    {
        return possible_keys[0];
    }
    throw runtime_error("Possible key not found");
}

int main(int argc, char *argv[])
{
    setlocale(LC_ALL, "Russian");
    // setlocale(LC_CTYPE, ".1251");

    if (argc < 2)
    {
        cout << "usage: " << argv[0] << " filename" << endl;
        exit(1);
    }
    string text = load_from_file(argv[1]);

    map<unsigned char, float> char_frequencies;
    freq_analysis(text, char_frequencies);
    // cout << endl << setprecision(2);
    // for (auto pair : char_frequencies)
    // {
    //     cout << "'" << pair.first << "'" << ": " << pair.second * 100 << "%" << endl;
    // }
    // cout << endl;
    
    // vector<unsigned char> possible_keys;
    // match_frequencies(char_frequencies, avg_freq_characters, possible_keys);
    try
    {
        unsigned char possible_key = find_possible_key(char_frequencies);
        cout << "Possible key is: " << bitset<8>(possible_key) << endl;
        return 0;
    }
    catch (runtime_error e)
    {
        cerr << e.what() << endl;
        return 1;
    }
}