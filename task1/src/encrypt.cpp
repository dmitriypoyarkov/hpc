#include "encrypt_lib.cpp"

int main(int argc, char *argv[])
{
    setlocale(LC_ALL, "Russian");

    unsigned char key;
    string filename;
    parse_args(argc, argv, filename, key);

    string origin = load_from_file(filename);
    string encrypted = apply_8bit_xor(origin, key);

    string out_filename = remove_extension(filename) + "_enc.txt";
    save_to_file(out_filename, encrypted);
    cout << "Encrypted to file: " << out_filename << endl;

    // string decrypted = apply_8bit_xor(encrypted, key);

    // cout << "Key: " << bitset<8>((int)key) << endl
    //      << endl;
    // cout << "Origin: " << endl
    //      << origin << endl
    //      << endl;
    // cout << "Encrypted: " << endl
    //      << encrypted << endl
    //      << endl;
    // cout << "Decrypted: " << endl
    //      << decrypted << endl
    //      << endl;

    // cout << "Match: " << (origin == decrypted ? "Yes" : "No") << endl;
    return 0;
}