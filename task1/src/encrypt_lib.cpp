#include <iostream>
#include <string>
#include <bitset>
#include <fstream>
#include <sstream>
#include <cstring>
#include <cmath>
#include <map>
#include <vector>

using std::string, std::cout, std::endl, std::cin,
    std::bitset, std::getline, std::ifstream,
    std::stringstream, std::ofstream, std::strlen,
    std::pow, std::map, std::vector;

void print_bytes(string str)
{
    for (auto chr : str)
    {
        cout << bitset<8>((int)chr) << ' ';
    }
    cout << endl;
}

string apply_8bit_xor(string origin, unsigned char key)
{
    size_t len = origin.length();
    string encrypted = string(len, ' ');
    for (int i = 0; i < len; i++)
    {
        encrypted[i] = origin[i] ^ key;
    }
    return encrypted;
}

string load_from_file(string filename)
{
    ifstream f(filename);
    if (!(f.is_open()))
    {
        std::cout << "file open error";
        exit(1);
    }
    stringstream buffer;
    buffer << f.rdbuf();
    f.close();
    return buffer.str();
}

void save_to_file(string filename, string data)
{
    ofstream f(filename);
    if (f.bad())
    {
        std::cout << "file write error";
        exit(1);
    }
    f << data;
    f.close();
}

void parse_args(int argc, char *argv[], string &filename_arg,
        unsigned char &key)
{
    if (argc < 3)
    {
        cout << "usage: " << argv[0] << " file key" << endl;
        exit(1);
    }
    filename_arg = argv[1];

    if (strlen(argv[2]) != 8)
    {
        cout << "key argument must be of length 8" << endl;
        exit(1);
    }
    key = 0;
    unsigned int bit;
    for (int i = 0; i < 8; i++)
    {
        bit = argv[2][7-i] - '0';
        if (bit > 1)
        {
            cout << "key argument must consist of ones and zeros" << endl;
            exit(1);
        }
        key += pow(2, i) * bit;
    }
}


void freq_analysis(const string &text, map<unsigned char, float> &out_char_frequencies)
{
    out_char_frequencies.clear();
    for (char ch : text)
    {
        out_char_frequencies[ch] += 1;
    }
    int len = text.length();
    for (auto iter = out_char_frequencies.begin(); 
            iter != out_char_frequencies.end(); iter++)
    {
        iter->second /= len;
    }
}

void match_frequencies(const map<unsigned char, float> &char_frequencies,
        const map<float, unsigned char> &avg_freq_characters,
        vector<unsigned char> &out_possible_keys)
{
    float freq = 0.;
    float cur_delta = 1.;
    float cur_min_delta = 1.;
    unsigned char cur_best_match = 0;
    for (auto avg_pair : avg_freq_characters)
    {
        freq = avg_pair.first;
        cur_best_match = 0;
        cur_min_delta = 0;
        for (auto data_pair : char_frequencies)
        {
            cout << data_pair.second << endl;
            cur_delta = fabs(data_pair.second - freq);
            if (cur_delta < cur_min_delta)
            {
                cur_min_delta = cur_delta;
                cur_best_match = data_pair.first;
            }
        }
        out_possible_keys.push_back(avg_pair.second ^ cur_best_match);
    }
}

std::string remove_extension(const std::string& filename) {
    size_t lastdot = filename.find_last_of(".");
    size_t lastslash = filename.find_last_of("\\");
    if (lastdot == std::string::npos)
    {
        return filename;
    }
    if (lastslash != string::npos and lastdot < lastslash)
    {
        return filename;
    }
    return filename.substr(0, lastdot);
}